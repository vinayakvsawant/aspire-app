## Endpoints

- Register as user/admin
	Users can register themselves as customers or admin
	`api/register` (POST)

- Login for user/ admin
	Users can login using the authentication token 
	`api/login` (POST)

- Logout for user/admin
	Logout from the application 
	`api/logout` (POST)

- Create a loan application with a user
	Create a new loan application with loan amount and repayment period 
	`api/logout` (POST)

- Get all loan applications
	Fetch all applications in the system (for admin), or fetch applications made by a user (for customer)
	`get_all_applications` (GET)

- Approve a loan application 
	Approve a loan application by providing rate of interest and loan id 
	api/approve_application (POST)

- Reject a loan application
	Reject an application by providing loan id and reason for rejection 
	`api/reject_application` (POST)
- Check next payment 
	Customer can check his next payment instalment status, due date, overdue amount if any, last payment date and amount 
	`api/check_next_payment` (POST)

- Check payment status
	Customer can check total instalments paid
	`api/check_payment_status` (POST)

- Make payment 
	Customer can make an instalment payment towards the loan amount
	`api/make_repayment` (POST)