<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\LoanApplication;
use Laravel\Sanctum\Sanctum;

class LoanApplicationTest extends TestCase
{
    use RefreshDatabase;
    /**
    * @test
    * new loan application with valid data
    *
    * @return void
    */
    public function new_application_with_valid_data()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['customer']
        );

        $faker = \Faker\Factory::create();

        $data = [
            'loan_amount' => $faker->numberBetween(1000, 100000),
            'loan_term' => $faker->numberBetween(1, 1000)
        ];

        $response = $this->post('api/create_new_application', $data, ['Accept' => 'application/json']);
        $response->assertSessionHasNoErrors()
             ->assertStatus(201);
    }

    /**
    * @test
    * New loan application with invalid data
    *
    * @return void
    */
    public function new_application_with_invalid_data()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['customer']
        );

        $faker = \Faker\Factory::create();

        $data = [
            'loan_amount' => $faker->numberBetween(1000, 100000),
            'loan_term' => $faker->text($maxNbChars = 50),
        ];

        $response = $this->post('api/create_new_application', $data, ['Accept' => 'application/json']);
        $response->assertStatus(422);
    }

    /**
    * @test
    * Admin to reject a loan application
    *
    * @return void
    */
    public function reject_loan_application()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['admin']
        );

        $loanApplication = LoanApplication::factory()->create([]);

        $data = [
            'application_id' => $loanApplication->id,
            'comment' => 'Documents are not sufficient'
        ];

        $response = $this->post('api/reject_application', $data, ['Accept' => 'application/json']);
        $response->assertStatus(201);
    }

    /**
    * @test
    * Admin to reject a loan application without comment
    *
    * @return void
    */
    public function reject_loan_application_without_comment()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['admin']
        );

        $loanApplication = LoanApplication::factory()->create([]);

        $data = [
            'application_id' => $loanApplication->id
        ];

        $response = $this->post('api/reject_application', $data, ['Accept' => 'application/json']);
        $response->assertStatus(422);
    }

    /**
    * @test
    * Customer to reject a loan application without comment
    *
    * @return void
    */
    public function reject_loan_application_with_customer_role()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['customer']
        );

        $loanApplication = LoanApplication::factory()->create([]);

        $faker = \Faker\Factory::create();

        $data = [
            'application_id' => $loanApplication->id,
            'comment' => $faker->text($maxNbChars = 50),
        ];

        $response = $this->post('api/reject_application', $data, ['Accept' => 'application/json']);
        $response->assertStatus(401);
    }

    /**
    * @test
    * Admin to approve loan application
    *
    * @return void
    */
    public function approve_loan_application()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['admin']
        );

        $loanApplication = LoanApplication::factory()->create([]);

        $faker = \Faker\Factory::create();

        $data = [
            'application_id' => $loanApplication->id,
            'rate_of_interest' =>  $faker->randomDigit(),
        ];

        $response = $this->post('api/approve_application', $data, ['Accept' => 'application/json']);
        $response->assertStatus(201);
    }

    /**
    * @test
    * Admin to approve loan application without rate of interest
    *
    * @return void
    */
    public function approve_loan_application_without_rate_of_interest()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['admin']
        );

        $loanApplication = LoanApplication::factory()->create([]);

        $data = [
            'application_id' => $loanApplication->id
        ];

        $response = $this->post('api/approve_application', $data, ['Accept' => 'application/json']);
        $response->assertStatus(422);
    }

    /**
    * @test
    * Customer to approve loan application
    *
    * @return void
    */
    public function approve_loan_application_with_customer_role()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['customer']
        );

        $loanApplication = LoanApplication::factory()->create([]);
        $faker = \Faker\Factory::create();

        $data = [
            'application_id' => $loanApplication->id,
            'rate_of_interest' => $faker->randomDigit(),
        ];

        $response = $this->post('api/approve_application', $data, ['Accept' => 'application/json']);
        $response->assertStatus(401);
    }


}
