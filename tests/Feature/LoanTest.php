<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use App\Models\User;
use App\Models\Loan;
use App\Models\Repayment;

class LoanTest extends TestCase
{
    use RefreshDatabase;
    /**
    * @test
    * check next payment with valid data
    *
    * @return void
    */
    public function check_next_payment_with_valid_data()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['customer']
        );


        $loan = Loan::factory()->create([
            'customer_id' => auth()->user()->id,
        ]);

        $data = [
            'loan_id' => $loan->id
        ];

        $response = $this->post('api/check_next_payment', $data, ['Accept' => 'application/json']);

        $response->assertSessionHasNoErrors()
             ->assertStatus(201);
    }

    /**
    * @test
    * check next payment with invalid data
    *
    * @return void
    */
    public function check_next_payment_with_invalid_data()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['customer']
        );

        // getting loan accounts of other customers
        $loan = Loan::factory()->create([
            'customer_id' => rand(2,10)
        ]);

        $data = [
            'loan_id' => $loan->id
        ];

        $response = $this->post('api/check_next_payment', $data, ['Accept' => 'application/json']);
        
        $response->assertSessionHasNoErrors()
             ->assertStatus(401);
    }

    /**
    * @test
    * check payment status with valid data
    *
    * @return void
    */
    public function check_payment_status_with_valid_data()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['customer']
        );


        $loan = Loan::factory()->create([
            'customer_id' => auth()->user()->id,
        ]);

        $repayment = Repayment::factory()->create([
            'loan_id' => $loan->id,
        ]);
        
        $data = [
            'loan_id' => $loan->id
        ];

        $response = $this->post('api/check_payment_status', $data, ['Accept' => 'application/json']);

        $response->assertSessionHasNoErrors()
             ->assertStatus(201);
    }

    /**
    * @test
    * check payment status with invalid data
    *
    * @return void
    */
    public function check_payment_status_with_invalid_data()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['customer']
        );


        $loan = Loan::factory()->create([
            'customer_id' => rand(2,10),
        ]);

        $data = [
            'loan_id' => $loan->id
        ];

        $response = $this->post('api/check_next_payment', $data, ['Accept' => 'application/json']);
        
        $response->assertSessionHasNoErrors()
             ->assertStatus(401);
    }
}
