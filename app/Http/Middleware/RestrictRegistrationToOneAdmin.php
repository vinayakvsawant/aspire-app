<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RestrictRegistrationToOneAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->role == 1) {
            $user_roles = DB::table('users')->pluck('role')->toArray();
            if (!empty($user_roles) && in_array(1, $user_roles)) {
                // fail if we already have a user with admin role
                return response([
                    'message' => 'Cannot register more than 1 administrator'
                ], 401);
            }
        }
        return $next($request);
    }
}
