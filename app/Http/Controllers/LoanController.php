<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LoanApplication;
use App\Models\Loan;
use App\Models\Repayment;
use Carbon\Carbon;
use App\Http\Library\ApiHelpers;

class LoanController extends Controller
{
	use ApiHelpers;

    public function createApplication(Request $request)
    {
    	$fields = $request->validate([
    		'loan_amount' => 'required|regex:/^\d+(\.\d{1,2})?$/|gt:0',
    		'loan_term' => 'required|numeric|gt:0',
    	]);

        // Admin can create application for any user by passing `customer_id`
        if ($this->isAdmin($request->user())) {
            if(isset($request->customer_id) && $request->customer_id != auth()->user()->id) {
                $customer_id = $request->customer_id;
            }
            else return $this->onError(401, 'This operation is not authorized');
        }
        else $customer_id = auth()->user()->id;

    	$loanApplication = LoanApplication::create([
    		'customer_id' => auth()->user()->id,
    		'loan_amount' => $fields['loan_amount'],
	        'loan_term' => $fields['loan_term'],
	        'status' => "Processing",
	        'description' => $request->description
    	]);

    	$response = [
    		'data' => $loanApplication,
    		'message' => 'Loan application submitted'
    	];

    	return response($response, 201);
    }

    public function getAllApplications(Request $request)
    {
        if($this->isAdmin($request->user())) {
            $loanApplications = LoanApplication::all();
        }
        else {
            $loanApplications = LoanApplication::where('customer_id', auth()->user()->id)->get();
        }
        $response = [
                'data' => $loanApplications,
                'message' => 'Fetched successfully'
        ];
        return response($response, 201);
    }

    public function approveApplication(Request $request)
    {
    	if (!$this->isAdmin($request->user())) {
            return $this->onError(401, 'Unauthorized Access');
        }

    	$fields = $request->validate([
    		'application_id' => 'required|numeric',
    		'rate_of_interest' => 'required|regex:/^\d+(\.\d{1,2})?$/|gt:0|lt:100'
    	]);

    	$loanApplication = LoanApplication::where('id', $fields['application_id'])->first();

    	if (!$loanApplication) {
    		return response([
    			'message' => 'Application cannot be found in the system'
    		], 404);
    	}
    	else {
    		$loanApplication->status = 'Approved';
    		$loanApplication->update();

    		$loan = Loan::create([
    			'loan_amount' => $loanApplication->loan_amount,
		        'application_id' => $loanApplication->id,
		        'customer_id' => $loanApplication->customer_id,
		        'rate_of_interest' => $fields['rate_of_interest'],
		        'loan_term' => $loanApplication->loan_term,
		        'approved_by' => auth()->user()->id
    		]);

    		$response = [
	    		'data' => $loan,
	    		'message' => 'Loan approved'
	    	];

	    	return response($response, 201);
    	}
    }

    public function rejectApplication(Request $request)
    {
    	if (!$this->isAdmin($request->user())) {
            return $this->onError(401, 'Unauthorized Access');
        }

    	$fields = $request->validate([
    		'application_id' => 'required|numeric',
    		'comment' => 'required'
    	]);

    	$loanApplication = LoanApplication::where('id', $fields['application_id'])->first();

    	if (!$loanApplication) {
    		return response([
    			'message' => 'Application cannot be found in the system'
    		], 404);
    	}
    	else {
    		$loanApplication->status = 'Rejected';
    		$loanApplication->comment = $fields['comment'];
    		$loanApplication->update();

    		$response = [
	    		'data' => $loanApplication,
	    		'message' => 'Loan application rejected successfully'
	    	];

	    	return response($response, 201);
    	}
    }

    public function makeRepayment(Request $request)
    {
    	$fields = $request->validate([
    		'loan_id' => 'required|numeric',
    		'installment_amount' => 'required|numeric|gt:0'
    	]);

    	$customer_id = auth()->user()->id;

    	$loan = Loan::where('id', $fields['loan_id'])->first();

    	if ($loan->customer_id !== $customer_id) {
    		return response([
    			'message' => 'Loan account cannot be accessed by current user'
    		], 401);
    	}
    	else {
            // check if loan amout is greater than previous installments and current payment
            $pastPayment = $installments = Repayment::where('loan_id', $loan->id)->sum('installment_amount');
            $total_payment = $fields['installment_amount'] + $pastPayment;
            if ($loan->loan_amount >= $total_payment) {
                $repayment = Repayment::create($fields);
                $response = [
                    'data' => $repayment,
                    'message' => 'Installment paid successfully'
                ];

                return response($response, 201);
            }
            else {
                return response([
                    'message' => 'Loan amount is less than payment'
                ], 401);

            }
    	}

    }

    public function checkNextPayment(Request $request)
    {
    	$fields = $request->validate([
    		'loan_id' => 'required|numeric'
    	]);

    	$customer_id = auth()->user()->id;

    	$loan = Loan::where('id', $fields['loan_id'])->first();

        if (!$loan) {
            return response([
                'message' => 'Loan account cannot be found'
            ], 404);
        }
    	elseif (!$this->isAdmin($request->user()) && ($loan->customer_id != $customer_id)) {
    		return response([
    			'message' => 'Loan account cannot be accessed by current user'
    		], 401);
    	}
    	else {
    		$installment = $this->calculateInstallment($loan->loan_amount, $loan->rate_of_interest, $loan->loan_term);
    		$due_date = Carbon::parse($loan->created_at)->addDays(7);
    		$overdue_amount = 0;
    		
    		$lastInstallment = Repayment::where('loan_id', $loan->id)->latest('created_at')->first();
    		if (!$lastInstallment) {
    			$last_payment_date = null;
    			$last_payment_amount = null;
    		}
    		else {
    			$last_payment_date = $lastInstallment->created_at;
    			$last_payment_amount = $lastInstallment->installment_amount;
    		}

    		$data = [
    			'loan_id' => $loan->id,
    			'next_installment' => $installment,
    			'due_date' => $due_date,
    			'overdue_amount' => $overdue_amount,
    			'last_payment_date' => $last_payment_date,
    			'last_payment_amount' => $last_payment_amount,
    		];
    		$response = [
	    		'data' => $data,
	    		'message' => 'Fetched successfully'
	    	];

	    	return response($response, 201);
    	}
    }

    public function checkPaymentStatus(Request $request)
    {
    	$fields = $request->validate([
    		'loan_id' => 'required|numeric'
    	]);

    	$customer_id = auth()->user()->id;

    	$loan = Loan::where('id', $fields['loan_id'])->first();

    	if (!$loan) {
            return response([
                'message' => 'Loan account cannot be found'
            ], 404);
        }
        elseif (!$this->isAdmin($request->user()) && ($loan->customer_id != $customer_id)) {
    		return response([
    			'message' => 'Loan account cannot be accessed by current user'
    		], 401);
    	}
    	else {
    		$installments = Repayment::where('loan_id', $loan->id)->get();
    		if ($installments->isEmpty()) {
    			return response([
	    			'message' => 'No payment done for this loan'
	    		], 404);
    		}
    		else {
	    		$response = [
		    		'data' => $installments,
		    		'message' => 'Fetched successfully'
		    	];
	    		 
	    	return response($response, 201);
	    	}
	    }

    }

    private function calculateInstallment($loan, $interest, $tenure) {
    	$tenure = $tenure > 0 ? $tenure : 1;
        $totalRepaymentRequired = $loan * (1 + ($interest * $tenure));
        return $totalRepaymentRequired / $tenure;
    }
}
