<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{	
    public function register(Request $request)
    {
    	$fields = $request->validate([
    		'name' => 'required|string',
    		'email' => 'required|string|email|unique:users,email',
    		'password' => 'required|string',
    		'role' => 'required',
    	]);

    	$user = User::create([
    		'name' => $fields['name'],
    		'email' => $fields['email'],
    		'password' => Hash::make($fields['password']),
    		'role' =>$fields['role'],
    	]);

    	$role = $user->role == 1 ? 'admin' : 'customer';

    	$token = $user->createToken('auth_token', [$role])->plainTextToken;

    	$response = [
    		'user' => $user,
    		'token' => $token
    	];

    	return response($response, 201);
    }

    public function login(Request $request)
    {
    	$fields = $request->validate([
    		'email' => 'required|email',
    		'password' => 'required|string',
    	]);

    	// Check email
    	$user = User::where('email', $fields['email'])->first();

        // Check password
    	if(!$user || !Hash::check($fields['password'], $user->password)) {
    		return response([
    			'message' => 'Bad credentials'
    		], 401);
    	}

    	$role = $user->role == 1 ? 'admin' : 'customer';

    	$token = $user->createToken('auth_token', [$role])->plainTextToken;

    	$response = [
    		'user' => $user,
    		'token' => $token
    	];

    	return response($response, 201);
    }

    public function logout(Request $request)
    {
    	auth()->user()->tokens()->delete();

    	return [
    		'message' => 'Logged out'
    	];
    }
}
