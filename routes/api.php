<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\LoanController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Protected routes
Route::group(['middleware' => ['auth:sanctum']], function() {
	Route::post('/logout', [AuthController::class, 'logout']);

	Route::post('/check_next_payment', [LoanController::class, 'checkNextPayment']);
	Route::post('/check_payment_status', [LoanController::class, 'checkPaymentStatus']);
	Route::get('/get_all_applications', [LoanController::class, 'getAllApplications']);
	

	// Routes for customer
	Route::post('/create_new_application', [LoanController::class, 'createApplication']);
	Route::post('/make_repayment', [LoanController::class, 'makeRepayment']);


	// Routes for admin
	Route::post('/approve_application', [LoanController::class, 'approveApplication']);
	Route::post('/reject_application', [LoanController::class, 'rejectApplication']);
});

// Public routes
Route::post('register', [AuthController::class, 'register'])->middleware('restrictadminregistration');
Route::post('login', [AuthController::class, 'login']);
