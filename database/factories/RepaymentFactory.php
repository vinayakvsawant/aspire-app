<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Loan;

class RepaymentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $loan_id = Loan::count() > 1 ? $this->faker->unique()->numberBetween(1, Loan::count()) : 1;
        return [
            'loan_id' => $loan_id,
            'installment_amount' => $this->faker->numberBetween(1000, 10000),
        ];
    }
}
