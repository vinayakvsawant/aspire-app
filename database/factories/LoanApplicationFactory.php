<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class LoanApplicationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'customer_id' => $this->faker->unique()->numberBetween(1, User::count()),
            'loan_amount' => $this->faker->numberBetween(1000, 10000000),
            'loan_term' => $this->faker->randomDigit(),
            'status' => 'Processing',
        ];
    }
}
