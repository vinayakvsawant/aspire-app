<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\LoanApplication;
use App\Models\User;

class LoanFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $application_id = LoanApplication::count() > 1 ? $this->faker->unique()->numberBetween(1, LoanApplication::count()) : 1;
        $customer_id = User::count() > 1 ? $this->faker->unique()->numberBetween(1, User::count()) : 1;
        return [
            'application_id' => $application_id,
            'customer_id' => $customer_id,
            'loan_amount' => $this->faker->numberBetween(1000, 10000000),
            'rate_of_interest' => $this->faker->randomDigit(),
            'loan_term' => $this->faker->randomDigit(),
            'approved_by' => 1,
        ];
    }
}
